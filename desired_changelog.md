[//]: # (This is the proposed &#40;desired&#41; output. Please feel free suggest other useful items)
[//]: # (See: https://about.gitlab.com/blog/2018/07/03/solving-gitlabs-changelog-conflict-crisis/)

[//]: # (our release version numbers are generated from the branch name: release/yyyy.mm.dd. Pull from that)
# 2023.11.13
- 2023-11-14 - This is the description of my second changelog entry
- 2023-11-13 - This is the description of my first changelog entry
